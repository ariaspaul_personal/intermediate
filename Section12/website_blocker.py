import datetime
from datetime import datetime as dt
import time


hosts_temp = 'hosts'
hosts_path = '/etc/hosts'
redirect = '127.0.0.1'
website_list = ['www.facebook.com','facebook.com']

year = dt.now().year
month = dt.now().month
day   = dt.now().day

while True:
	# is time between 8am and 4PM?
	time.sleep(1) # seconds
	if dt(year,month,day,8) < dt.now() < dt(year,month,day,16):
		print('Working hours...')
		with open(hosts_path,'r+') as file:
			content=file.read()
			for website in website_list:
				if website in content:
					pass
				else:
					file.write(redirect+' '+website+'\n')
			
	else:
		print('Personal time...')
		with open(hosts_path,'r+') as file:
			content = file.readlines()
			file.seek(0)
			for line in content:
				if not any(website in line for website in website_list):
					file.write(line)
			file.truncate()

